import React from 'react';
import styles from './App.scss';
import MainPage from "./pages/main/MainPage"
import LoginPage from "./pages/login/LoginPage"
import {BrowserRouter, Route, Switch} from "react-router-dom";

class App extends React.Component{

  render() {
    return (
        <div className="App">
          <div>
              <BrowserRouter>
                <Switch>
                    <Route path='/login' component={LoginPage}></Route>
                    <Route path='/' component={MainPage}></Route>
                </Switch>
              </BrowserRouter>
          </div>
        </div>
    )
  }
}

export default App;
