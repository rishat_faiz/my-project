import * as React from "react";
import style from './LoginComponent.module.scss'

export default class LoginComponent extends React.Component{

    render() {
        return (
            <form className={style.loginComponent__container}>
                <div className={style.loginComponent__container_header}>
                    Войти
                </div>
                <input  className={[style.loginComponent__field, style.loginComponent__login_field].join(' ')} type='input' placeholder='Введите логин' required/>
                <input className={[style.loginComponent__field, style.loginComponent__password_field].join(' ')} type='input' placeholder='Введите пароль' required/>
                <input className={style.loginComponent__container_submit} type='submit'/>
            </form>
        )
    }
}
