import * as React from "react";
import LoginComponent from "../../component/login/LoginComponent";
import style from './LoginPage.module.scss'

class LoginPage extends React.Component{

    render() {
        return (
            <div className={style.loginPage__container}>
                <LoginComponent/>
            </div>
        )
    }
}

export default LoginPage;
