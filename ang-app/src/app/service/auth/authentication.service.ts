import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Person} from "../../component/login-form/LoginModel";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private httpClient: HttpClient) { }

  sendLogin(person:Person) {
    return this.httpClient.post("/api/login", JSON.stringify(person))
        .subscribe(data => console.log(data))
  }
}
