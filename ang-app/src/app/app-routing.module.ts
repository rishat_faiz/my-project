import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginPageComponent} from "./page/login-page/login-page.component";
import {HomePageComponent} from "./page/home-page/home-page.component";


const routes: Routes = [
  {path: 'login', component: LoginPageComponent },
  {path: 'home', component: HomePageComponent}
    ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
    LoginPageComponent,
    HomePageComponent
]