import { Component, OnInit } from '@angular/core';
import {Person, LoginConfig, LoginLabelConfig} from "../../component/login-form/LoginModel";
import {AuthenticationService} from "../../service/auth/authentication.service";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  private _loginFormConfig: LoginConfig = new LoginConfig();

  get loginConfig(): LoginConfig {
    return  this._loginFormConfig;
  }

  constructor(private authService: AuthenticationService) {
    this._loginFormConfig.title="Войти"
    this._loginFormConfig.loginConfig = new LoginLabelConfig("Пользователь", 'Введите имя пользователя');
    this._loginFormConfig.passwordConfig = new LoginLabelConfig('Пароль', 'Введите паоль пользователя')
  }

  ngOnInit() {

  }

  submit(person: Person) {
    console.log("eee")
    console.log(`login = ${person.login}; password = ${person.password}`)
    this.authService.sendLogin(person);
  }
}
