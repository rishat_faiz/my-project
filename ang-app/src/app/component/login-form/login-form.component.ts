import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Person, LoginConfig, LoginLabelConfig} from "./LoginModel";
import {config} from "rxjs";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  private _config:LoginConfig;

  @Output() submitForm = new EventEmitter<Person>()

 @Input("config")set config(value: LoginConfig) {
    this._config = value
  };

  get config() {
    return this._config;
  }

  public person: Person

  constructor() {
    this.person = new Person();
    this._config =  new LoginConfig();
    this._config.title = 'Login';
    this._config.loginConfig = new LoginLabelConfig("Login", "Enter your login")
    this._config.passwordConfig = new LoginLabelConfig("Password", "Enter your passord")
  }


  ngOnInit() {
  }

  submit() {

      this.submitForm.emit(this.person)
  }

}
