export class Person {
    constructor(private _login: string = '', private _password: string = '') {

    }

    get login(): string {
        return this._login;
    }

    set login(login:string) {
        this._login = login
    }

    get password(): string {
        return this._password
    }

    set password(password: string) {
        this._password = password
    }
}

export class LoginConfig {

    constructor(private _title:string = '', private _loginConfig: LoginLabelConfig = new LoginLabelConfig(),
                private _passwordConfig: LoginLabelConfig = new LoginLabelConfig()) {

    }

    get title(): string {
        return this._title
    }

    set title(title: string) {
        this._title = title
    }

    get loginConfig() : LoginLabelConfig {
        return this._loginConfig
    }

    set loginConfig(loginConfig: LoginLabelConfig) {
        this._loginConfig = loginConfig;
    }

    get passwordConfig(): LoginLabelConfig {
        return this._passwordConfig
    }

    set passwordConfig(passwordConfig: LoginLabelConfig) {
        this._passwordConfig = passwordConfig
    }
}

export class LoginLabelConfig {

    constructor(private _title: string = '',
                private _helpTitle:  string = '') {

    }

    get title(): string {
        return this._title
    }

    set title(title: string) {
        this._title = title
    }

    get helpTitle(): string {
        return this._helpTitle
    }

    set helpTitle(title: string) {
        this._helpTitle = title
    }
}
