import VueRouter from 'vue-router'
import HomePage from './../page/HomePage'
import LoginPage from './../page/LoginPage'
import Vue from "vue";

Vue.use(VueRouter)

let routes = [
    {path: '/login', name: 'LoginPage', component: LoginPage},
    {path: '/*', name: 'HomePage', component: HomePage}
]

export default new VueRouter({
    routes
})
